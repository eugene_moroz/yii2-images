<?php

namespace jekamoroz\yii2images\models;

use jekamoroz\yii2images\ModuleTrait;
use Yii;
use yii\helpers\BaseFileHelper;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $owner_type
 *
 * @property GalleryImage[] $galleryImages
 */
class Gallery extends \yii\db\ActiveRecord
{
    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'owner_type'], 'required'],
            [['owner_id'], 'integer'],
            [['owner_type'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'owner_type' => 'Owner Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGalleryImages()
    {
        return $this->hasMany(GalleryImage::className(), ['gallery_id' => 'id']);
    }
}
